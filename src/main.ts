import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as session from 'express-session';
import * as passport from 'passport';
import { ConfigService } from '@nestjs/config';
import { HttpStatus, ValidationPipe, VersioningType } from '@nestjs/common';
import { createClient } from 'redis';
import * as createRedisStore from 'connect-redis';
import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';
import * as bodyParser from 'body-parser';
import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppClusterService } from './app-cluster.service';
import { TransformInterceptor } from './interceptors/global.intercepter';


async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  /*app.use(csurf({ sessionKey: 'demo-session' }));*/

  app.setGlobalPrefix('api/');
  app.use((req, res, next) => {
    res.header('x-powered-by', 'Blood, Tears & Gold');
    next();
  });
  app.enableVersioning({
    type: VersioningType.URI,
  });

    app.useGlobalPipes(
      new ValidationPipe({
        transform: true,
      }),
    );

  const configService = app.get(ConfigService);

  const RedisStore = createRedisStore(session);
  const redisClient = createClient({
    host: configService.get('REDIS_HOST'),
    port: configService.get('REDIS_PORT'),
    password: configService.get('REDIS_PASSWORD'),
  });

  app.use(
    bodyParser.urlencoded({
      extended: true,
    }),
  );
  app.use(
    session({
      store: new RedisStore({ client: redisClient }),
      secret: configService.get('SESSION_SECRET'),
      resave: false,
      saveUninitialized: false,
      cookie: {
        maxAge: 8600000 * 3600,
      },
    }),
  );
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 100, // limit each IP to 100 requests per windowMs
    }),
  );
  app.enableCors({
    optionsSuccessStatus: HttpStatus.NO_CONTENT,
    credentials: true,
    origin: 'http://217.25.93.11',
  });

  app.use(helmet());
  /*app.useGlobalInterceptors(new TransformInterceptor());*/

  app.useStaticAssets(join(__dirname, '..', 'assets/public'));
  app.setBaseViewsDir(join(__dirname, '..', 'assets/view'));
  app.setViewEngine('hbs');

  await app.listen(3500);
}
bootstrap();
/*AppClusterService.clusterize(bootstrap);*/
