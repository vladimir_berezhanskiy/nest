import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectlistService } from './projectlist.service';
import { ProjectlistController } from './projectlist.controller';
import Project from './projectlist.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Project])],
  providers: [ProjectlistService],
  exports: [ProjectlistService],
  controllers: [ProjectlistController],
})
export class ProjectlistModule {}
