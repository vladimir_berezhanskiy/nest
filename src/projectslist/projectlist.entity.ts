import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany, ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import User from '../users/user.entity';
import Task from '../task/task.entity';
import Project from "../projects/project.entity";
import Company from "../company/company.entity";

export enum Status {
  In_Progress = 'In Progress',
  Ready = 'Ready',
  Behind = 'Behind',
}

@Entity()
class ProjectList {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

 /* @OneToMany(() => User, (user: User) => user.company)
  @JoinColumn()
  public users: User[];
  @ManyToMany(type => User, user => user.projects, { cascade: true })*/

  /*@ManyToMany(() => Project, (project: Project) => project.projectList, {
    cascade: true,
  })
  @JoinTable()
  public projects: Project[];*/

  @Column()
  public title: string;

  @Column()
  public color: string;


  @OneToMany(() => Project, (project: Project) => project.projects)
  @JoinColumn()
  public projects: Project[];


/*  @OneToMany(() => Project, (project: Project) => project.)
  @JoinColumn()
  public projects: Task[];*/

  constructor(props) {
    Object.assign(this, props);
  }
}

export default ProjectList;
