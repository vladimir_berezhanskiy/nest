import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Req,
  UseGuards,
  UseInterceptors,
  Version,
} from '@nestjs/common';
import { ThrottlerGuard } from '@nestjs/throttler';
import { CookieAuthenticationGuard } from '../authentication/cookieAuthentication.guard';
import { RolesGuard } from '../roles/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { Role } from '../roles/role.enum';
import RequestWithUser from '../authentication/requestWithUser.interface';
import { ProjectlistService } from './projectlist.service';
import createProjectDto from './dto/createProject.dto';


@Controller({
  version: '1',
  path: 'projectlist',
})

@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(ThrottlerGuard)
export class ProjectlistController {
  constructor(private readonly projectService: ProjectlistService) {}

  @HttpCode(HttpStatus.OK)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Get()
  async getProject(@Req() request: RequestWithUser) {
    return await this.projectService.getAllProjects(request?.user.id);
  }

  @HttpCode(HttpStatus.CREATED)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Post()
  async createProject(
    @Req() request: RequestWithUser,
    @Body() createProjectDto: createProjectDto,
  ) {
    return await this.projectService.createProject(
      request?.user.id,
      request.body,
    );
  }

  @HttpCode(HttpStatus.OK)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Get()
  async findOneProject(@Req() request: RequestWithUser, @Param() params: createProjectDto) {
    console.log(request.params.id);
    return await this.projectService.findOneProject(
      request?.user.id,
      request?.params.id,
    );
  }

  @HttpCode(HttpStatus.OK)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Delete()
  async deleteProject(
    @Req() request: RequestWithUser,
    @Body() createProjectDto: createProjectDto,
  ) {
    return await this.projectService.deleteProject(
      request?.user.id,
      request.body.id,
    );
  }

  @HttpCode(HttpStatus.OK)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Patch()
  async updateProject(
    @Req() request: RequestWithUser,
    @Body() createProjectDto: createProjectDto,
  ) {
    return await this.projectService.updateProject(
      request?.user.id,
      request.body,
    );
  }
}
