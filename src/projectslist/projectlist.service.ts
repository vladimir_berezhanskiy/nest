import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { createQueryBuilder, Repository } from 'typeorm';
import Project from './projectlist.entity';
import createProjectDto from './dto/createProject.dto';
import User from '../users/user.entity';
import ProjectList from './projectlist.entity';

@Injectable()
export class ProjectlistService {
  constructor(
    @InjectRepository(ProjectList)
    private projectRepository: Repository<ProjectList>,
  ) {}

  /* const findProject = await createQueryBuilder(Project, 'project') пример как искать 1 проект
     .leftJoin('project.users', 'user')                               текущего пользователя по id проекта
     .where('project.id IN(:ids)', { ids: project.id })               и id этого пользователя
     .andWhere('user.id IN(:ids1)', { ids1: userId })
     .getOne();*/

  async getAllProjects(id: string) {
    return await createQueryBuilder(ProjectList, 'project')
      .leftJoinAndSelect('project.projects', 'projects')
      .leftJoinAndSelect('projects.members', 'members')
      .leftJoinAndSelect('projects.tasks', 'tasks')
      .where('members.id IN(:id)', { id })
      .getMany();
  }

  /* const myEntities = await this.projectRepository
     .createQueryBuilder('project')
     .leftJoinAndSelect('project.projectsList', 'project')
  /!*   .leftJoinAndSelect('projectlist.projectsList', 'projects')*!/
/!*     .leftJoinAndSelect('projects.members', 'members')
     .leftJoinAndSelect('projects.tasks', 'tasks')*!/
    /!* .leftJoinAndSelect('projects.tasks', 'user')*!/
     .where('user.id IN(:ids)', { ids: id })
     .getMany();
   if (myEntities) {
     return myEntities;
   }
   throw new HttpException(
     'User with this id does not exist',
     HttpStatus.NOT_FOUND,
   );*/

  async createProject(id: string, dto: createProjectDto) {
    /*  const newProject = await this.projectRepository.create(dto);
      const user = new User({
        id,
      });
      newProject.members = [user];
      return await this.projectRepository.save(newProject);*/
  }

  async deleteProject(id: string, projectId: string) {
    const findProject = await createQueryBuilder(Project, 'project')
      .leftJoin('project.users', 'user')
      .where('project.id IN(:ids)', { ids: projectId })
      .andWhere('user.id IN(:ids1)', { ids1: id })
      .getOne();
    const myEntities = await this.projectRepository
      .createQueryBuilder('project')
      .leftJoin('project.users', 'user')
      .where('project.id IN(:ids)', { ids: projectId })
      .andWhere('user.id IN(:ids1)', { ids1: id })
      .getOne();
    if (myEntities) {
      return await this.projectRepository
        .delete(myEntities.id)
        .then((response) => response.raw[0]);
    }
    throw new HttpException('Project not Found', HttpStatus.NOT_FOUND);
  }

  async updateProject(userId: string, project: createProjectDto): Promise<any> {
    const findProject = await createQueryBuilder(ProjectList, 'project')
      .leftJoin('project.projects', 'projects')
      .where('project.id IN(:ids)', { ids: project.id })
      /*.andWhere('user.id IN(:ids1)', { ids1: userId })*/
      .getOne();
    console.log(findProject);
    if (findProject) {
      const updatedData = await createQueryBuilder('project', 'project')
        .update<Project>(Project, { ...project })
        .where('project.id = :id', { id: project.id })
        .returning('*') // returns all the column values
        .updateEntity(true)
        .execute();
      if (updatedData) {
        return updatedData.raw[0];
      }
      throw new HttpException(
        'Error to updade project',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
    throw new HttpException('Project not Found', HttpStatus.NOT_FOUND);
  }

  async findOneProject(userId: string, projectId: string): Promise<any> {
    console.log(projectId);
    const findProject = await createQueryBuilder(Project, 'project')
      .leftJoinAndSelect('project.members', 'user')
      .leftJoinAndSelect('project.tasks', 'task')
      .where('project.id IN(:ids)', { ids: projectId })
      .andWhere('user.id IN(:ids1)', { ids1: userId })
      .getOne();
    if (findProject) {
      return findProject;
    } else {
      throw new HttpException('Project not Found', HttpStatus.NOT_FOUND);
    }
  }
}
