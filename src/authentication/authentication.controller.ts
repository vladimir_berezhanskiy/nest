import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpCode,
  Post,
  Req,
  Res, Session,
  UseGuards,
  UseInterceptors
} from "@nestjs/common";
import { AuthenticationService } from './authentication.service';
import RegisterDto from './dto/register.dto';
import RequestWithUser from './requestWithUser.interface';
import { CookieAuthenticationGuard } from './cookieAuthentication.guard';
import { LogInWithCredentialsGuard } from './logInWithCredentials.guard';
import { ThrottlerGuard } from '@nestjs/throttler';
import { DoesUserExist } from './doesUserExist.guard';
import { EmailConfirmationGuard } from '../email/emailConfirmation.guard';
import { Response } from 'express';
import * as uuid from 'uuid4';


@Controller({
  version: '1',
})
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(ThrottlerGuard)
export class AuthenticationController {
  constructor(private readonly authenticationService: AuthenticationService) {}

  @Post('auth/register')
  @UseGuards(DoesUserExist)
  async register(@Body() registrationData: RegisterDto) {
    /*await this.emailConfirmationService.sendVerificationLink(
      registrationData.email,
    );*/
    return await this.authenticationService.register(registrationData);
  }

  @HttpCode(200)
  /* @UseGuards(EmailConfirmationGuard)*/
  @UseGuards(LogInWithCredentialsGuard)
  @Post('auth/login')
  async logIn(
    @Session() ses: any,
    @Req() request: RequestWithUser,
    @Res({ passthrough: true }) response: Response,
  ) {
    const id = uuid();
    response.cookie('isAuth', id, { maxAge: 1000000 * 3600, httpOnly: false });
    return request.user;
  }

  @HttpCode(200)
  @UseGuards(CookieAuthenticationGuard)
  @Get()
  async authenticate(@Req() request: RequestWithUser) {
    return request.user;
  }

  @HttpCode(200)
  @UseGuards(CookieAuthenticationGuard)
  @Post('auth/logout')
  async logOut(
    @Req() request: RequestWithUser,
    @Res({ passthrough: true }) response: Response,
  ) {
    request.logOut();
    request.session.cookie.maxAge = 0;
    response.clearCookie('isAuth');
  }
}
