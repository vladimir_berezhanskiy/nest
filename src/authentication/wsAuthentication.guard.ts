import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ChatService } from '../chat/chat.service';

@Injectable()
export class WsAuthenticationGuard implements CanActivate {
  constructor(private chatservice: ChatService) {}

  async canActivate(context: ExecutionContext) {
    const client = context.switchToWs().getClient();
    const user = await this.chatservice.getUserFromSocket(client);
    return Boolean(user);
  }
}

