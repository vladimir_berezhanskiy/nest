import {
  Column,
  Entity,
  JoinColumn, ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn
} from "typeorm";
import { Exclude, Transform } from "class-transformer";
import { Role } from '../roles/role.enum';
import Company from '../company/company.entity';
import Project from "../projects/project.entity";


@Entity()
export class User {
/*  public get userRole() {
    return this.roles;
  }*/

  @PrimaryGeneratedColumn('uuid')
  public id: string; // id пользователя

  @Column({ unique: true })
  public email: string; //email уникальный

  @Column({ nullable: true })
  public name: string; //Имя

  @Column({ nullable: true })
  public surname: string; //Фамилия

  @Column({ nullable: true })
  public patronymic: string; //Отчество

  @Column({ unique: true, nullable: true })
  public username: string; //username уникальный

  @Column()
  @Exclude()
  public password: string; //пароль

  @ManyToOne(() => Company, (company: Company) => company.users)
  @JoinColumn()
  public company: Company;

  @Column({ nullable: true })
  public phoneNumber: string; //телефон

  @ManyToMany(() => Project, (project: Project) => project.members, {
  })
  public projects: Project[];

  @Column({
    type: 'enum',
    enum: Role,
    default: Role.User,
  })
  public roles: Role; //Роли

  @Column({ default: false })
  public isEmailConfirmed: boolean; //подтвержден ли емаил

  @Column({ nullable: true })
  public avatar: string; //avatar url

  @Column({ default: false })
  public isBanned: boolean; //забанен ли?

  @Column({ default: false })
  @Transform(
    (value) => (value.value = value.obj.roles === 'ADMIN' ? true : false),
  )
  public isAdmin: boolean; // админ ли?

  @Column({ default: true })
  public isActive: boolean; //Активный ли?

  constructor(props) {
    Object.assign(this, props);
  }
}

export default User;
