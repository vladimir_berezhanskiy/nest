import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import User from './user.entity';
import { UserController } from './user.controller';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { ConfigModule, ConfigService } from '@nestjs/config';
import EmailService from '../email/email.service';
import { EmailConfirmationService } from '../email/emailConfirmation.service';


@Module({
  imports: [
    PassportModule,
    ConfigModule,
    TypeOrmModule.forFeature([User]),
    JwtModule.register({}),
  ],
  providers: [UsersService, EmailService, EmailConfirmationService],
  exports: [UsersService],
  controllers: [UserController],
})
export class UsersModule {}
