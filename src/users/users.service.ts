import {
  ForbiddenException,
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import User from './user.entity';
import CreateUserDto from './dto/createUser.dto';
import { EmailConfirmationService } from '../email/emailConfirmation.service';


@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async findOneByEmail(email: string) {
    return await this.usersRepository.findOne({ where: { email } });
  }

  async getByEmail(email: string) {
    const user = await this.usersRepository.findOne({ email });
    if (user) {
      return user;
    }
    throw new HttpException(
      'User with this email does not exist',
      HttpStatus.NOT_FOUND,
    );
  }

  async getById(id: string) {
    const myEntities = await this.usersRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.company', 'company')
      .where('company.id IN(:ids)', { ids: id })
      .leftJoinAndSelect('user.projects', 'project')
      .leftJoinAndSelect('project.tasks', 'task')
      .leftJoinAndSelect('project.members', 'members')
      .where('user.id IN(:ids)', { ids: id })
      .getOne();
    if (myEntities) {
      return myEntities;
    }
    throw new NotFoundException('NotFound');
  }

  async inviteUserInCompany(id?: string, companyId?: any) {
    /* const payload =
    /!*  await this.emailConfirmationService.decodeConfirmationTokenInvite(token);*!/*/
    console.log(id);
    const user = await this.usersRepository.findOne(String(id));
    if (user) {
      return await this.usersRepository.update({ id }, { company: companyId });
    }
    throw new HttpException(
      'User with this id does not exist',
      HttpStatus.NOT_FOUND,
    );
  }

  async markEmailAsConfirmed(email: string) {
    return this.usersRepository.update(
      { email },
      {
        isEmailConfirmed: true,
      },
    );
  }

  async create(userData: CreateUserDto) {
    const newUser = await this.usersRepository.create(userData);
    await this.usersRepository.save(newUser);
    return newUser;
  }
}
