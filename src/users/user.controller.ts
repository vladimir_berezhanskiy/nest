import {
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpCode,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { CookieAuthenticationGuard } from '../authentication/cookieAuthentication.guard';
import { Roles } from '../roles/roles.decorator';
import { Role } from '../roles/role.enum';
import RequestWithUser from '../authentication/requestWithUser.interface';
import { RolesGuard } from '../roles/roles.guard';
import { ThrottlerGuard } from '@nestjs/throttler';


import { UsersService } from './users.service';
import { EmailConfirmationService } from '../email/emailConfirmation.service';

export interface userIdDto {
  token?: string;
  companyId?: string;
  email?: string;
  userId?: string;
}

@Controller({
  version: '1',
})
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(ThrottlerGuard)
export class UserController {
  constructor(
    private readonly userService: UsersService,
    private readonly emailConfirmationService: EmailConfirmationService,
  ) {}

  @HttpCode(200)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Get()
  async authenticate(@Req() request: RequestWithUser) {
    return request.user;
  }

  @HttpCode(200)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.User)
  @Get('user/me')
  async getMe(@Req() request: RequestWithUser) {
    return await this.userService.getById(request.user.id);
  }

  @HttpCode(200)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Get('/invite')
  async inviteUserInCompany(
    @Req() request: RequestWithUser,
    @Query() query: userIdDto,
  ) {
    if (query?.token && request.user.roles !== Role.Admin) {
      const token =
        await this.emailConfirmationService.decodeConfirmationTokenInvite(
          query?.token,
        );
      console.log(token);
      return await this.userService.inviteUserInCompany(
        token?.userId,
        token?.companyId,
      );
    }
    return await this.emailConfirmationService.sendInviteLink(
      query.email,
      query.userId,
      query.companyId,
    );
  }
}
