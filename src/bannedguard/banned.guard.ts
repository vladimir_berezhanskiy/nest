import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
  ForbiddenException,
} from '@nestjs/common';
import RequestWithUser from '../authentication/requestWithUser.interface';

@Injectable()
export class BannedGuard implements CanActivate {
  canActivate(context: ExecutionContext) {
    const request: RequestWithUser = context.switchToHttp().getRequest();

    if (request.user?.isBanned) {
      throw new ForbiddenException('Forbidden');
    }
    return true;
  }
}

