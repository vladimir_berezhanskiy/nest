import { IsNotEmpty, IsOptional, IsUUID } from 'class-validator';
export class createProjectDto {
  @IsNotEmpty()
  @IsUUID('4', { message: ':)' })
  id: string;
  @IsOptional()
  name: string;

  @IsOptional()
  desc: string;
}

export default createProjectDto;
