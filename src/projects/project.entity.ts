import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany, ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import User from '../users/user.entity';
import Task from '../task/task.entity';
import Company from "../company/company.entity";
import ProjectList from "../projectslist/projectlist.entity";


export enum Status {
  In_Progress = 'In Progress',
  Ready = 'Ready',
  Behind = 'Behind',
}

@Entity()
class Project {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

 /* @OneToMany(() => User, (user: User) => user.company)
  @JoinColumn()
  public users: User[];
  @ManyToMany(type => User, user => user.projects, { cascade: true })*/

  @ManyToMany(() => User, (user: User) => user.projects, {
    cascade: true,
  })
  @JoinTable()
  public members: User[];

  @Column()
  public name: string;

  @Column({ nullable: true})
  public desc: string;

  @Column({ nullable: true })
  public avatar: string;

  @Column({ type: 'enum', enum: Status, default: Status.In_Progress })
  public status: Status;

  @OneToMany(() => Task, (task: Task) => task.project)
  @JoinColumn()
  public tasks: Task[];

  @Column({ nullable: true })
  public deadline: Date;


  @ManyToOne(() => ProjectList, (projectlist: ProjectList) => projectlist.projects)
  @JoinColumn()
  public projects: ProjectList;

  constructor(props) {
    Object.assign(this, props);
  }
}

export default Project;
