import { CacheModule, Module } from '@nestjs/common';
import { ChatService } from './chat.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ChatGateway } from './chat.gateway';
import { TypeOrmModule } from '@nestjs/typeorm';
import Message from './message.entity';
import { PassportModule } from '@nestjs/passport';
import { AuthenticationService } from '../authentication/authentication.service';
import { UsersService } from '../users/users.service';
import User from '../users/user.entity';
import { RedisCacheService } from './redisCache.service';
import * as redisStore from 'cache-manager-redis-store';


@Module({
  imports: [
    PassportModule,
    ConfigModule,
    TypeOrmModule.forFeature([Message, User]),
    JwtModule.register({}),
    CacheModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        store: redisStore,
        host: configService.get('REDIS_HOST'),
        port: configService.get('REDIS_PORT'),
        password: configService.get('REDIS_PASSWORD'),
        ttl: 120,
      }),
    }),
  ],
  providers: [
    AuthenticationService,
    UsersService,
    ChatService,
    ChatGateway,
    RedisCacheService,
  ],
  exports: [ChatService],
})
export class ChatModule {}
