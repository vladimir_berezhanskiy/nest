import { Injectable } from '@nestjs/common';
import { AuthenticationService } from '../authentication/authentication.service';
import { InjectRepository } from '@nestjs/typeorm';
import Message from './message.entity';
import User from '../users/user.entity';
import { Repository } from 'typeorm';
import { Socket } from 'socket.io';
import { parse } from 'cookie';
import { WsException } from '@nestjs/websockets';
import { RedisCacheService } from './redisCache.service';
import * as cookieParser from 'cookie-parser';
import { ConfigService } from '@nestjs/config';
import { UsersService } from '../users/users.service';


@Injectable()
export class ChatService {
  constructor(
    private readonly authenticationService: AuthenticationService,
    @InjectRepository(Message)
    private messagesRepository: Repository<Message>,
    private redis: RedisCacheService,
    private configService: ConfigService,
    private userService: UsersService,
  ) {}

  async saveMessage(content: string, author: User) {
    const newMessage = await this.messagesRepository.create({
      content,
      author,
    });
    await this.messagesRepository.save(newMessage);
    return newMessage;
  }

  async getAllMessages() {
    return this.messagesRepository.find({
      relations: ['author'],
    });
  }

  async getUserFromSocket(socket: Socket) {
    const cookie = !!socket?.handshake?.headers?.cookie;
    if (cookie) {
      const parsedCookie = JSON?.parse(
        JSON?.stringify(
          cookieParser?.signedCookies(
            parse(socket?.handshake?.headers?.cookie),
            this.configService.get('SESSION_SECRET'),
          ),
        ),
      );
      if (parsedCookie) {
        const redis = await this.redis.get(`sess:${parsedCookie['connect.sid']}`);
        if (redis) {
          const userId = redis['passport']['user'];
          if (userId) {
            return await this.userService.getById(userId);
          }
          throw new WsException('Invalid credentials.');
        }
        throw new WsException('Invalid credentials.');
      } else throw new WsException('Invalid credentials.');
    } else throw new WsException('Invalid credentials.');
  }
}
