export interface TokenPayload {
  token: string;
  userId: string;
};

