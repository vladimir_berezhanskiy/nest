import { Module } from '@nestjs/common';
import EmailService from './email.service';
import { ConfigModule } from '@nestjs/config';
import { EmailConfirmationService } from './emailConfirmation.service';
import { JwtModule } from '@nestjs/jwt';
import { EmailController } from './email.controller';
import {UsersModule } from '../users/users.module';


@Module({
  imports: [ConfigModule, JwtModule.register({}), UsersModule],
  controllers: [EmailController],
  providers: [EmailService, EmailConfirmationService],
  exports: [EmailService, EmailConfirmationService],
})
export class EmailModule {}
