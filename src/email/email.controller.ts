import {
  Controller,
  ClassSerializerInterceptor,
  UseInterceptors,
  Get,
  Query,
  Render,
} from '@nestjs/common';
import ConfirmEmailDto from './confirmEmail.dto';
import { EmailConfirmationService } from './emailConfirmation.service';


@Controller('email-confirmation')
@UseInterceptors(ClassSerializerInterceptor)
export class EmailController {
  constructor(
    private readonly emailConfirmationService: EmailConfirmationService,
  ) {}

  @Get('confirm')
  @Render('emailconfirmed')
  async confirm(@Query() query: ConfirmEmailDto) {
    const email = await this.emailConfirmationService.decodeConfirmationToken(
      query.token,
    );
    const emailIs = await this.emailConfirmationService.confirmEmail(email);
    if (emailIs) {
      return { message: 'Email confirmed' };
    }
    return { message: 'Email already confirmed' };
  }
}
