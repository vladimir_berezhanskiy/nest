export enum Role {
  User = 'USER',
  Admin = 'ADMIN',
  Manager = 'MANAGER',
  Moderator = 'MODERATOR',
}
