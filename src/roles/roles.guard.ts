import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Role } from './role.enum';
import { ROLES_KEY } from './roles.decorator';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const requiredRoles = this.reflector.getAllAndOverride<Role[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (!requiredRoles) {

      return true;
    }
    const request = context.switchToHttp().getRequest();
    if (!request.isAuthenticated()) {

      throw new UnauthorizedException();
    }
    const user = request.user;
    if (!user) {

      throw new UnauthorizedException();
    }

    if (user.roles === 'ADMIN') {
      return true;
    }

    return requiredRoles.some((role) => {
      return user.roles?.includes(role);
    });
  }
}
