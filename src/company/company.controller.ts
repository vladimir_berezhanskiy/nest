import {
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpCode,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ThrottlerGuard } from '@nestjs/throttler';
import { CookieAuthenticationGuard } from '../authentication/cookieAuthentication.guard';
import { RolesGuard } from '../roles/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { Role } from '../roles/role.enum';
import RequestWithUser from '../authentication/requestWithUser.interface';
import { CompanyService } from './company.service';


@Controller({
  version: '1',
})
/*@Controller('user')*/
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(ThrottlerGuard)
export class CompanyController {
  constructor(private readonly companyService: CompanyService) {}

  @HttpCode(200)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Get('company')
  async authenticate(@Req() request: RequestWithUser) {
    return await this.companyService.getById(request.user?.company?.id);
  }

  @HttpCode(200)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.User)
  @Get('company/me')
  async getMe(@Req() request: RequestWithUser) {
    /*console.log(request.user.company.id);*/
    return await this.companyService.getById(request.user?.id);
  }
}
