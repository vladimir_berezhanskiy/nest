import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import Company from "./company.entity";


@Injectable()
export class CompanyService {
  constructor(
    @InjectRepository(Company)
    private companyRepository: Repository<Company>,
  ) {}

  async getById(id: string) {
    if (id) {
      const company = await this.companyRepository.findOne(String(id), {
        relations: ['users'],
      });
      if (company) {
        return company;
      }
    }
    throw new HttpException(
      'You are not a member of more than one company.',
      HttpStatus.NOT_FOUND,
    );
  }
}
