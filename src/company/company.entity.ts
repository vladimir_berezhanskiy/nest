import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import User from '../users/user.entity';


@Entity()
class Company {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @OneToMany(() => User, (user: User) => user.company)
  @JoinColumn()
  public users: User[];

  @Column()
  public name: string;
}

export default Company;
