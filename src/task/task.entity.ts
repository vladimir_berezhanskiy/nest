import {
  Column,
  Entity,
  JoinColumn, ManyToOne, OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import Project from "../projects/project.entity";


export enum Status {
  In_Progress = 'In Progress',
  Ready = 'Ready',
  Behind = 'Behind',
}


@Entity()
class Task {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @ManyToOne(() => Project, (project: Project) => project.tasks)
  @JoinColumn()
  public project: Project;

  @Column()
  public name: string;


  @Column({default: false})
  public ready: boolean;
}

export default Task;
