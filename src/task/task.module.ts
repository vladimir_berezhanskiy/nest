import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TaskService } from './task.service';
import { TaskController } from './task.controller';
import Task from './task.entity';
import Project from "../projects/project.entity";
import { ProjectService } from "../projects/project.service";

@Module({
  imports: [TypeOrmModule.forFeature([Task, Project])],
  providers: [TaskService, ProjectService],
  exports: [TaskService],
  controllers: [TaskController],
})

export class TaskModule {}
