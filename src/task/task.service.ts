import { HttpException, HttpStatus, Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from '@nestjs/typeorm';
import { createQueryBuilder, Repository } from 'typeorm';
import Task from './task.entity';
import createProjectDto from '../projects/dto/createProject.dto';
import Project from '../projects/project.entity';
import e from 'express';
import User from '../users/user.entity';
import CreateTaskDto from './dto/create.task';

@Injectable()

export class TaskService {
  constructor(
    @InjectRepository(Task)
    private tasksRepository: Repository<Task>,
    @InjectRepository(Project)
    private projectRepository: Repository<Project>,
  ) {}

  async getById(id: string) {
    if (id) {
      const tasks = await this.tasksRepository.find({
        relations: ['project'],
        /*where: [{ project: { users: id } }],*/
      });
      if (tasks) {
        return tasks;
      }
    }
    throw new HttpException(
      'User with this id does not exist',
      HttpStatus.NOT_FOUND,
    );
  }

  async update(userId: string, tasks: string): Promise<any> {
    const findTask = await createQueryBuilder(Task, 'task')
      .where('task.id IN(:ids)', { ids: tasks })
      .getOne();

    if (findTask) {
      return findTask;
    } else throw new HttpException('Task not Found', HttpStatus.NOT_FOUND);

    /*   if (findProject) {
      const updatedData = await createQueryBuilder('project', 'project')
        .update<Project>(Project, { ...project })
        .where('project.id = :id', { id: project.id })
        .returning('*') // returns all the column values
        .updateEntity(true)
        .execute();
      if (updatedData) {
        return updatedData.raw[0];
      }
      throw new HttpException('Error to updade project', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    throw new HttpException('Project not Found', HttpStatus.NOT_FOUND);*/
  }

  async createTask(id: string, task: CreateTaskDto) {
    console.log(id);
    const newTask = await this.tasksRepository.create(task);
    const project = await this.projectRepository.findOne({
      where: { id: task.projectId },
    });
    console.log(project);
    newTask.project = project;
    if (newTask && project) {
      return await this.tasksRepository.save(newTask);
    } else {
      throw new NotFoundException();
    }
  }
}
