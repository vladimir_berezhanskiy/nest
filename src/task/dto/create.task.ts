export class CreateTaskDto {
  projectId: string;
  name: string;
  desc?: string;
}

export default CreateTaskDto;
