import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpCode, HttpStatus, Param, Patch, Post,
  Req,
  UseGuards,
  UseInterceptors
} from "@nestjs/common";
/*import { ThrottlerGuard } from '@nestjs/throttler';*/
import { CookieAuthenticationGuard } from '../authentication/cookieAuthentication.guard';
import { RolesGuard } from '../roles/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { Role } from '../roles/role.enum';
import RequestWithUser from '../authentication/requestWithUser.interface';
import { TaskService } from './task.service';
import CreateTaskDto from './dto/create.task';


@Controller({
  version: '1',
})
/*@Controller('user')*/
@UseInterceptors(ClassSerializerInterceptor)
/*@UseGuards(ThrottlerGuard)*/
export class TaskController {
  constructor(private readonly tasksService: TaskService) {}

  @HttpCode(200)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Get('/task')
  async get(@Req() request: RequestWithUser) {
    return await this.tasksService.getById(request?.user.id);
  }


  @HttpCode(200)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Post('/task')
  async addTaskInProject(@Req() request: RequestWithUser, @Body() body: CreateTaskDto) {
    return await this.tasksService.createTask(
      request?.user.id,
      request.body,
    );
  }

  /*  @HttpCode(200)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.User)
  @Get('/me')
  async getMe(@Req() request: RequestWithUser) {
    /!*console.log(request.user.company.id);*!/
    return await this.projectService.getById(request.user.id);
  }*/

  @HttpCode(HttpStatus.OK)
  @UseGuards(CookieAuthenticationGuard)
  @UseGuards(RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Patch('task/:id')
  async updateTasks(@Req() request: RequestWithUser, @Param() params: any) {
    return await this.tasksService.update(
      request?.user.id,
      request?.params.id,
    );
  }
}
